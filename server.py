import socket
SERVER_ADDRESS = ('localhost', 8125)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(SERVER_ADDRESS)
client = []
print('Start Server')
while True:
    try: 
        data, addres = sock.recvfrom(1024)
        print(addres[0], addres[1])
    except socket.error: 
        pass 
    
    else:
        if addres not in client:
            client.append(addres)  # Если такого клиента нет, то добавить
        for clients in client:
            if clients == addres:
                continue  # Не отправлять данные клиенту который их прислал
            sock.sendto(data, clients)
